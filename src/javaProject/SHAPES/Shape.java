package SHAPES;

import java.io.*;
import java.util.*;

/**
 * 
 */
public abstract class Shape {

    /**
     * Default constructor
     */
    public Shape() {
    }

    /**
     * 
     */
    protected int x;

    /**
     * 
     */
    protected int y;

    /**
     * 
     */
    protected Color color;

    /**
     * 
     */
    protected int id;

    /**
     * 
     */
    protected int thinkness;

    /**
     * 
     */
    protected Boolean visibility;

    /**
     * 
     */
    protected char char_s;

    /**
     * @return
     */
    public ArrayList<Pixel> draw() {
        // TODO implement here
        return null;
    }

    /**
     * @return
     */
    public String toString() {
        // TODO implement here
        return "";
    }

    /**
     * @param newVisibility  
     * @return
     */
    public void setVisibility(Boolean newVisibility ) {
        // TODO implement here
        return null;
    }

}